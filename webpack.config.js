const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = () => {
  const isProduction = !!process.env.NODE_ENV;
  console.info('Production: ', isProduction);

  return {
    entry: [
      './src/index.ts',
      './src/sass/index.scss',
    ],
    output: {
      filename: '[name].js',
      path: `${path.resolve(
        __dirname
      )}/dist`,
      publicPath: '/dist',
    },
    devtool: isProduction ? 'none' : 'source-map',
    optimization: {
      minimize: isProduction,
      minimizer: [new TerserPlugin()],
      mergeDuplicateChunks: true,
      removeEmptyChunks: false,
      splitChunks: {
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all',
          },
        },
      },
    },
    module: {
      rules: [
        // perform js babelization on all .js files
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['babel-preset-env'],
            },
          },
        },
        {
          test: /\.s(c|a)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { importLoaders: 1 } },
            {
              loader: 'postcss-loader',
            },
            {
              loader: 'sass-loader',
            },
          ],
        },
        {
          test: /\.(eot|ttf|woff|woff2|otf)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[contenthash:8].[ext]',
                outputPath: '/assets/fonts',
                useRelativePath: true,
              },
            },
          ],
        },
        {
          test: /\.(png|jpe?g|gif|svg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[contenthash:8].[ext]',
                outputPath: '/assets/images',
                useRelativePath: true,
              },
            },
            {
              loader: 'image-webpack-loader',
            },
          ],
        },
      ],
    },
    plugins: [
      // extract css into dedicated file
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[name].css',
      }),
      new webpack.LoaderOptionsPlugin({
        options: {
          postcss: [autoprefixer()],
        },
      }),
    ],
  };
};
