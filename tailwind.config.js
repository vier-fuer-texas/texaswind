const isProduction = !!process.env.NODE_ENV;

module.exports = {
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
  purge: {
    enabled: isProduction,
    content: ['./demo/**/*.html', './src/**/*.ts'],
    options: {
      whitelist: [
        // 'xl:transform',
      ],
    },
  },
  theme: {
    fontFamily: {
      condensed: ['Oswald regular', 'Arial Narrow', 'sans-serif'],
      sans: ['Open Sans', 'sans-serif'],
      copy: [
        'Open Sans',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
    },
    extend: {
      fontSize: {
        '7xl': '5rem',
      },
      colors: {
        gray: {
          100: '#f6f6f6',
          200: '#ebebeb',
          300: '#d9d9d9',
          400: '#BFBFBF',
          500: '#9e9e9e',
          600: '#7F7F7F',
          700: '#595959',
          800: '#424242',
          900: '#2D2926',
          default: '#f6f6f6',
        },
        primary: {
          lighter: '#8BFB97',
          default: '#00915A',
          darker: '#005001',
          darkest: '#074231',
        },
        'dark-blue': '#1D2C38',
        turquoise: '#52CDC5',
        'light-blue': '#4BA1BA',
        'light-green': '#A3C439',
        'link-hover-green': '#97BF0D',
        purple: '#BA3075',
      },
      inset: {
        '4rem': '4rem',
      },
      opacity: {
        85: '0.85',
        95: '0.95',
      },
    },
    container: {
      center: true,
      padding: '1rem',
      screens: {
        sm: '100%',
        md: '100%',
        lg: '40rem',
        xl: '60rem',
      },
    },
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'active'],
  },
  plugins: [
    // eslint-disable-next-line global-require
    require('@tailwindcss/typography'),
  ],
};
