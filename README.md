# Tailwind Webpack Boilerplate

![Texaswind](texaswindbanner.jpg)

A starting point for tailwind.css with SASS, autoprefixer, webpack, purging (tree-shaking) and some basic settings to get your next project started at no time.

- typescript
- webpack
- postcss
- node-sass
- terser
- babel
- linting & formatting (prettier, eslint, air-bnb)
- browserslist
- polyfills
  - object-fit-images