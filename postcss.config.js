const isProduction = !!process.env.NODE_ENV;

/* eslint-disable global-require */
module.exports = {
  plugins: [
    require('tailwindcss'), // converts @tailwind base to css in index.scss file
    require('autoprefixer'),
    require('postcss-object-fit-images'),
    isProduction && require('cssnano'),
  ],
};
